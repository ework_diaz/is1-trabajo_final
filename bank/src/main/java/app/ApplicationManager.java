package app;

public class ApplicationManager {
	private static ApplicationManager instance = null;
	
	public static ApplicationManager getInstance() {
		if (instance == null) {
			instance = new ApplicationManager();
		}
		return instance;
	}
	
	
}
