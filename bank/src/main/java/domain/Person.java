package domain;
import java.sql.Date;

import domain.Warehouse;

public class Person {
	private static int num=0;
	private int id_Person;
	private String firstName;
	private String lastName;
	private String document;
	private String warehouse;
	private int type;
	private boolean statusAccount;
	
	public Person(){
		num++;
		id_Person=num;		
	}
	public boolean modificar_informacion(Person usuario){
		return true;
	}
	public boolean resetearContrasena(String last_password,String new_password){
		return true;
	}
	public boolean activateAccount(Person usuario){
		return statusAccount;
	}
	
	public void existencePurchaseOrder(Date date,Date endDate,int quantity,float price){	
	}
	public void entryProductRegistration(DeliveryNote order){
	}
	public ProductCategory newTypeProductRegistration(String name){
	}
	public void reportPurchaseOrder_PendingForMonth(int month){
	}
	public void reportPurchaseOrder_CompleteForMonth(int month){
	}
	public void reportPurchaseOrder_AllForMonth(int month){
	}
	
	public void departureProductRegistration(){
	}
	public void reportEconomicMovement(Product product){
	}

}