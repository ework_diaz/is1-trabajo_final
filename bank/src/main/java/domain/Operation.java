package domain;

import java.sql.Timestamp;

public class Operation {
	public enum Type {
		WITHDRAWAL, DEPOSIT, TRANSFERENCE
	}
	
	private Integer id;
	private Timestamp date;
	private Type type;
	private Account account;
	private Account targetAccount;
	private Double balance = 0.0;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public Account getTargetAccount() {
		return targetAccount;
	}
	public void setTargetAccount(Account targetAccount) {
		this.targetAccount = targetAccount;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
}
