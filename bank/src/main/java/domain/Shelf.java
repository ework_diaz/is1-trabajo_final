package domain;
import domain.Row;

public class Shelf {
	private String name;
	private Row row;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Row getRow() {
		return row;
	}
	
	public void setRow(Row row) {
		this.row = row;
	}
	
	@Override
	public String toString(){
		return this.row + "Shelf: " + this.name + " ";
	}
}
