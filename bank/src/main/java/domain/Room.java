package domain;
import domain.Warehouse;

public class Room {
	private String name;
	private Warehouse warehouse;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Warehouse getWarehouse() {
		return warehouse;
	}
	
	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}
	
	@Override
	public String toString(){
		return this.room + "Room: " + this.name + " ";
	}
}
