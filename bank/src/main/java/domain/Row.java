package domain;
import domain.Room;

public class Row {
	private String name;
	private Room room;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Room getRoom() {
		return room;
	}
	
	public void setRoom(Room room) {
		this.room = room;
	}
	
	@Override
	public String toString(){
		return this.row + "Row: " + this.name + " ";
	}
}
