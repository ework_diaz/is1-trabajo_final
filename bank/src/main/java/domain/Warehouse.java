package domain;

public class Warehouse {
	private String name;
	private double latitude;
	private double longitude;
	private double address;
	
	public Warehouse() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getAddress() {
		return address;
	}

	public void setAddress(double address) {
		this.address = address;
	}
	
	@Override
	public String toString(){
		return "Warehouse: " + this.name + " ";
	}
	
}
