package repository.inmemory;

import repository.AccountRepository;
import repository.OperationRepository;
import repository.PersonRepository;
import repository.RepositoryFactory;

public class InMemoryRespotoryFactory implements RepositoryFactory {

	@Override
	public AccountRepository createAccountRepository() {
		return new InMemoryAccountRepository();
	}

	@Override
	public PersonRepository createPersonRepository() {
		return null;
	}

	@Override
	public OperationRepository createOperationRepository() {
		// TODO Auto-generated method stub
		return null;
	}

}
