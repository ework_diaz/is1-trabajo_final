package repository.jdbc;

import repository.AccountRepository;
import repository.OperationRepository;
import repository.PersonRepository;
import repository.RepositoryFactory;

public class JdbcRespotoryFactory implements RepositoryFactory {

	@Override
	public AccountRepository createAccountRepository() {
		return new JdbcAccountRepository();
	}

	@Override
	public PersonRepository createPersonRepository() {
		return null;
	}

	@Override
	public OperationRepository createOperationRepository() {
		// TODO Auto-generated method stub
		return null;
	}

}
